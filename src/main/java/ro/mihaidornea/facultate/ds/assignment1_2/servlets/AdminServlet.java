package ro.mihaidornea.facultate.ds.assignment1_2.servlets;

import com.fasterxml.jackson.databind.ObjectMapper;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.DVD;
import ro.mihaidornea.facultate.ds.assignment1_2.service.ProducerService;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AdminServlet extends HttpServlet {

    private ProducerService producerService;


    @Override
    public void init() throws ServletException {
        producerService = new ProducerService();
        super.init();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        super.service(req, res);
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

    }

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String title = request.getParameter("title");
        int year = Integer.parseInt(request.getParameter("year"));
        double price = Double.parseDouble(request.getParameter("price"));

        DVD dvd = new DVD(title, year, price);
        ObjectMapper mapper = new ObjectMapper();
        String dvdJsonString = mapper.writeValueAsString(dvd);
        producerService.produceDVD(dvdJsonString);
    }

    @Override
    public void destroy() {

    }
}
