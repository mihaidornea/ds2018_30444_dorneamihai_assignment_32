package ro.mihaidornea.facultate.ds.assignment1_2;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.mihaidornea.facultate.ds.assignment1_2.entities.DVD;
import ro.mihaidornea.facultate.ds.assignment1_2.service.FileWriterService;
import ro.mihaidornea.facultate.ds.assignment1_2.service.MailService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

public class ConsumerTwo {

    private static final String EXCHANGE_NAME = "dvd";
    private static MailService mailService;
    @SuppressWarnings("Duplicates")
    public static void main(String[] args) throws IOException, TimeoutException, NoSuchAlgorithmException, KeyManagementException, URISyntaxException {
        mailService = new MailService("username", "password");
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqp://lwcytnyh:gfvzY6UbdZMmV--fKq_ektaoyR0Anizk@bee.rmq.cloudamqp.com/lwcytnyh");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare(EXCHANGE_NAME, "fanout");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "");

        System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

        Consumer consumer = new DefaultConsumer(channel) {
            @Override
            public void handleDelivery(String consumerTag, Envelope envelope,
                                       AMQP.BasicProperties properties, byte[] body) throws IOException {
                String message = new String(body, StandardCharsets.UTF_8);
                ObjectMapper mapper = new ObjectMapper();
                DVD dvd = mapper.readValue(message, DVD.class);
                mailService.sendMail("mdornea97@gmail.com", "DVD", dvd.toString());
                System.out.println(" [x] Received '" + dvd.toString() + "'");
            }
        };
        channel.basicConsume(queueName, true, consumer);
    }
}