package ro.mihaidornea.facultate.ds.assignment1_2.service;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileWriterService {
    public FileWriterService() {
    }

    public void writeMessageToFile(String fileTitle, String message){
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileTitle));
            writer.write(message);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
