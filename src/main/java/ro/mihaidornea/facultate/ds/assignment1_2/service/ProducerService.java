package ro.mihaidornea.facultate.ds.assignment1_2.service;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

public class ProducerService {

    private static final String EXCHANGE_NAME = "dvd";

    public void produceDVD(String dvdJsonString){
        ConnectionFactory factory = new ConnectionFactory();
        try {
            factory.setUri("amqp://lwcytnyh:gfvzY6UbdZMmV--fKq_ektaoyR0Anizk@bee.rmq.cloudamqp.com/lwcytnyh");
            Connection connection = factory.newConnection();
            Channel channel = connection.createChannel();

            channel.exchangeDeclare(EXCHANGE_NAME, "fanout");

            channel.basicPublish(EXCHANGE_NAME, "", null, dvdJsonString.getBytes());
            System.out.println(" [x] Sent '" + dvdJsonString + "'");

            channel.close();
            connection.close();
        } catch (URISyntaxException | NoSuchAlgorithmException | KeyManagementException | TimeoutException | IOException e) {
            e.printStackTrace();
        }
    }

}
