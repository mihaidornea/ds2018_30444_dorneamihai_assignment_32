package ro.mihaidornea.facultate.ds.assignment1_2.entities;


import com.fasterxml.jackson.annotation.JsonProperty;

public class DVD {

    @JsonProperty
    private String title;
    @JsonProperty
    private int year;
    @JsonProperty
    private double price;

    public DVD(String title, int year, double price) {
        this.title = title;
        this.year = year;
        this.price = price;
    }

    public DVD() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "DVD: " +
                title  +
                ", year=" + year +
                ", price=" + price;
    }
}
